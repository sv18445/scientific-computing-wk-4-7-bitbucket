# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 11:59:24 2018

@author: phoeb
"""
import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt 
from scipy import integrate

def mass_spring(X,t,pars): # setting up differential equation x'' + 2*xi*x' + kx = sin(pi*t)
    return [X[1],np.sin(np.pi*t) - 2*pars[1]*X[1] - pars[0]*X[0]]

def duffing(X,t,pars): # setting up differential equation x'' + 2*xi*x' + kx + beta*x^3= gamma*sin(pi*t)
    return [X[1], pars[2]*np.sin(np.pi*t) - 2*pars[1]*X[1] - pars[0]*X[0] - pars[3]*(X[0])**3]

# defining parameters:
xi = 0.05
gamma = 0.5
beta = 1
k=1 # initially set a value of k otherwise the program errors saying k is not defined
pars = [k, xi, gamma, beta]

T = 2 # We know the period is 2 for both mass_spring and duffing

## Shooting code
def zero_problem(s, ode, T, pars): # this is our root finding problem
    int_over_T = integrate.odeint(ode,s,[0,T], args=(pars,))
    last_x = int_over_T[-1,:] # takes the last value in the period (it a vector containing x(T) and x'(T))
    return s - last_x

def shooting(ode, sol_guess, T, pars):
    solution = fsolve(zero_problem,sol_guess, args = (ode, T, pars,))
    return solution
## Shooting code end

## General continuation set up code
# Note each point e.g first, second ,previous, current, next_pt etc is three dimensional
# point[0] = k value
# point[1] = initial condition x(0) corresponding to the k value
# point[2] = initial condition x'(0) corresponding to the k value
def generate_start_points_shooting(k1, k2, x0, ode): # takes two values of the parameter, an inital guess [x(0), x'(0)] and the ode
    k = k1
    first_IC = shooting(ode, x0, T, [k, xi, gamma, beta])
    first = np.concatenate([[k],first_IC])
    k = k2
# The second point using the initial conditions from the first as the starting guess for shooting
    second_IC = shooting(ode, first_IC, T, [k, xi, gamma, beta]) 
    second = np.concatenate([[k],second_IC])
    return first, second

def secant(previous, current): # This simply linearly extends two points by the secant method
    prediction = 2*current - previous
    return prediction

## Natural parameter continuation correction method
def correct_natural_continuation(next_pt, previous, ode):
    k = next_pt[0]
    old_IC = previous[1:3]
    new_IC = shooting(ode, old_IC, T, [k, xi, gamma, beta])
    corrected_pt = np.concatenate([[k],new_IC])
    return corrected_pt


## Psuedo arc length conditions
def tangent_condition(corrected, current, previous, predicted):
    return np.dot((corrected - predicted),(current - previous))

# system to solve for psuedo arclength 
def system_to_solve(point, current, previous, predicted, ode):
    conditionA = tangent_condition(point,current, previous, predicted)
    conditionA = np.array([conditionA])
    conditionB = zero_problem(point[1:3], ode, T, [point[0], xi, gamma, beta])
    conditions = np.concatenate([conditionA,conditionB])
    return conditions

def MAX(k, IC, ode, T): # This function calculates the max x value over a period, given a k value & initial conditions
    t = np.linspace(0,T,1000)
    solution = integrate.odeint(ode, IC, t, args = ([k, xi, gamma, beta],))
    solution = solution[:,0]
    max_x = max(solution)
    return max_x

# This function carries out the continuation on a given ODE
# You also need to specify whether to use natural_continuation or psuedo_continuation
def continuation(ode, x0, continuation_method, discretization, step_size):
    k_sol_array = []
    x_sol_array = []
    
    h = step_size 
    
    # Setting up our first and second points, here they are called previous and current respectively 
    # so that we can iterate using them in the while loop
    if discretization == "shooting":
        if continuation_method != "natural_continuation" and continuation_method != "psuedo_continuation":
            print ("Invalid continuation method")
        else:
            previous = generate_start_points_shooting(0.1, 0.1+h, x0, ode)[0]
            current = generate_start_points_shooting(0.1, 0.1+h, x0, ode)[1] 
            
            # Here we keep track of the max values and k values at each point
            x_sol_array.append(MAX(previous[0], previous[1:3], ode, T))
            x_sol_array.append(MAX(current[0], current[1:3], ode, T))
            k_sol_array.append(0.1)
            k_sol_array.append(0.1+h)
            
            while current[0] < 20: #this means the loop repeats until k reaches k=20
                predicted = secant(previous, current) # predicted point
                if continuation_method == "natural_continuation":
                    corrected_pt = correct_natural_continuation(predicted, previous, ode) # corrected point using
                elif continuation_method == "psuedo_continuation":
                    corrected_pt = fsolve(system_to_solve, predicted, args = (current, previous, predicted, ode),)
                previous = current
                current = corrected_pt
                max_x = MAX(corrected_pt[0], corrected_pt[1:3], ode, T)
                k_sol_array.append(corrected_pt[0])
                x_sol_array.append(max_x)
            plt.plot(k_sol_array, x_sol_array)
            plt.xlabel("k")
            plt.ylabel("x max")
            plt.xlim(0.1,20)
            if ode == mass_spring:
                    ode_name = "Mass Spring"
            elif ode == duffing:
                ode_name = "Duffing"
            else:
                ode_name = "Unknown ODE"
            if continuation_method == "natural_continuation":
                method_name = "Natural parameter"
            elif continuation_method == "psuedo_continuation":
                method_name = "Psuedo arclength"
            plt.title(method_name + " continuation for " + ode_name + " ODE")
            return k_sol_array, x_sol_array
    elif discretization == "collocation":
        print("Unfortunately I couldn't get collocation working in time")
    else:
        print("Invalid discretization method")


# Plotting results for mass_spring & duffing equation for both natural continuation and psuedo arc length
# Showing the different outputs of the continuation function
x0 = [0,1]
step_size = 0.1

continuation(duffing, x0, "psuedo_continuation", "collocation", step_size)
continuation(duffing, x0, "psuedo_continuation", "blahblah", step_size)
continuation(duffing, x0, "blahblahblah", "shooting", step_size)

plt.figure(0)
continuation(mass_spring, x0, "natural_continuation", "shooting", step_size)
plt.figure(1)
continuation(mass_spring, x0, "psuedo_continuation", "shooting", step_size)
plt.figure(2)
continuation(duffing, x0, "natural_continuation", "shooting", step_size)
plt.figure(3)
continuation(duffing, x0, "psuedo_continuation", "shooting", step_size)


# investigate the pararmeter plot of duffing equation for different values of gamma
plt.figure(4)
gamma = 0.1
continuation(duffing, x0, "psuedo_continuation", "shooting", step_size)
gamma = 0.2
continuation(duffing, x0, "psuedo_continuation", "shooting", step_size)
gamma = 0.4
continuation(duffing, x0, "psuedo_continuation", "shooting", step_size)
gamma = 0.8
continuation(duffing, x0, "psuedo_continuation", "shooting", step_size)

"""
This is the bit to run the unseen ODE:
Replace Unseen_ODE with the name of the function, and pick an x0 and step_size if desired
Otherwise it will run with x0 = [0,1] and step_size = 0.1 as defined in lines 143&144
I chose psuedo continuation because it works better than natural and shooting since collocation
doesn't work
continuation(Unseen_ODE, x0, "psuedo_continuation", "shooting", step_size)
"""