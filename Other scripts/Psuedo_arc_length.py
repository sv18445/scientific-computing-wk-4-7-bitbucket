# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 11:59:24 2018

@author: phoeb
"""
import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt 

"""
Psuedo arclength continuation
 x^3 - x + c = 0 
 c between [-2,2]
For this one, we change f to be a function of X only, where X = [x,c] this is because
when it comes to fsolve, we gain equations to solve for x & c, not just x as before
We rename this function F
"""
def F(X):
    return X[0]**3 - X[0] + X[1]

def f(x,c):
    X = [x,c]
    return F(X)

def tangent_condition(next_pt, current, previous, prediction):
    return np.dot((next_pt - prediction),(current - previous))

def system_to_solve(X, F, current, previous, prediction): 
    conditionA = tangent_condition(X,current, previous, prediction)
    conditionB = F(X)
    return (conditionA,conditionB)

def predict(current, previous): # note current_point and previous_point must be numpy arrays
    next_pt = 2*current - previous
    return next_pt

def generate_start_values(f, c1, c2):
    c = c1
    x_previous = fsolve(f,1,args = c)
    previous = np.array([float(x_previous), c])
    c = c2
    x_current = fsolve(f,1,args = c)
    current = np.array([float(x_current), c])
    return current, previous

def psuedo_arc(F, current, previous):
    x_sol_array = []
    c_array = []
    x_sol_array.append(previous[0])
    c_array.append(previous[1])
    x_sol_array.append(current[0])
    c_array.append(current[1])
    
    j = 1
    while j < 60:    
        prediction = predict(current,previous)
        next_pt = fsolve(system_to_solve,current, args = (F, current, previous, prediction),)
        previous = current
        current = next_pt
        x_sol_array.append(next_pt[0])
        c_array.append(next_pt[1])
        j = j+1
    return x_sol_array, c_array 
        
current = generate_start_values(f,-2,-1.9)[0]
previous = generate_start_values(f,-2,-1.9)[1]

x_sol_array = psuedo_arc(F, current, previous)[0]
c_array = psuedo_arc(F, current, previous)[1]

plt.figure(1, figsize = (8,8))
plt.plot(c_array, x_sol_array)
plt.title("Psuedo arclength continuation for $f(x,c) = x^3 - x + c$")
plt.xlabel("c")
plt.ylabel("x values s.t. f(x,c) = 0")
plt.xlim(-2,2)
plt.show()
