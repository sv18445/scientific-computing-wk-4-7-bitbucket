# -*- coding: utf-8 -*-
"""
Created on Wed Oct 24 11:23:23 2018

@author: sv18445
"""
import numpy as np
import math
from scipy import integrate
import matplotlib.pyplot as plt

"""
QUESTION 1
Duffing equation
First we split the second order Duffing equation into a system of first order ODES
X = [u,u']
and define this as a function, duffing - which takes three arguments,
X - a 2D array, t - a single number and pars - an array of the parameters in the duffing eqn
"""

def duffing(X,t,pars):
    return [X[1], pars[1]*math.sin(pars[2]*t) - 2*pars[0]*X[1] - X[0] - (X[0])**3]

# defining parameters:
xi = 0.05
gamma = 0.2
omega = 1

"""
This function below takes the starting conditions, a 2D array, the line colour for the plot
and l, a label for the plot.
It then plots the solution
"""    
def plot_solution(X0,colour,l):
    xs = np.linspace(0,50*np.pi,10000)
    solution = integrate.odeint(duffing,X0,xs, args=([xi, gamma, omega],))
    ys = solution[:,0]
    plt.plot(xs,ys,colour, label = l)
    return solution
    

"""
We also set some initial conditions in order to simulate our ODE, here I've 
created a grid of plots in which the initial condition varies.
"""
plt.figure(0)
plt.figure(figsize = (10,10))
plt.suptitle("Duffing equation with different initial conditions")

count = 1
for i in range(3):
    for j in range(3):
        plt.subplot(3,3,count)
        plot_solution([i,j],'-r','')
        title = [i,j]
        plt.title(title)
        count = count+1
        if j == 0:
            plt.ylabel("x")
        if i == 2:
            plt.xlabel("t")      
plt.show()

"""    
Another way of simulating the Duffing equation is to look at the phase plane, a plot of x against x'
"""
# Initial conditions [1,0]
xs = np.linspace(0,50*np.pi,10000)
solution = integrate.odeint(duffing,[1,0],xs, args=([xi, gamma, omega],))
plt.figure(1)
plt.plot(solution[:,0],solution[:,1])
plt.xlabel(r'$x$')
plt.ylabel(r"$x'$")
plt.title("Phase portrait")
plt.show()

"""
We see from figure 0 that as time tends to infinity, we gain periodic orbits.
We now want to isolate a periodic orbit.
We are going to do this for initial condition [1,0]
"""
plt.figure(2)
plot_solution([1,0],'-r','')
plt.title("Duffing equation, omega = 1, IC = [1,0]")
#From this graph we see that the solution settles down to periodic orbits after
#around t=80 
#So we now limit the x range of the plot to t=80 and t=90
plt.figure(3)
plot_solution([1,0],'-r','')
plt.title("Duffing equation, omega = 1, IC = [1,0]")
plt.xlim(80, 90)
#We can zoom in on this further to isolate one period
plt.figure(4)
plot_solution([1,0],'-r','')
plt.title("Duffing equation, omega = 1, IC = [1,0]")
plt.xlim(81.5, 88.5)
plt.axhline(y=0, color='b', linestyle='-')
#Here we have added a y=0 line to help identify one period
plt.figure(5)
plot_solution([1,0],'-r','')
plt.title("Duffing equation, omega = 1, IC = [1,0]")
plt.xlim(81.95, 88.3)
plt.axhline(y=0, color='b', linestyle='-')

"""
The above code gives us approximately one period (athough this is done by eye -
so not strictly accurate). 
Starting condition is approx [81.95,0]
but we want to find the periodic orbit exactly - note we know the period is 2*pi/omega
which is 2*pi in our case

we look at the corresponding U values at the start and end of the period
We know that the period is 2*pi since omega = 1
"""

U_val_at_start = integrate.odeint(duffing,[1,0],[0,81.95], args=([xi, gamma, omega],))
U_val_at_start = U_val_at_start[:,0][1]
U_val_at_end = integrate.odeint(duffing,[1,0],[0,81.95+2*np.pi], args=([xi, gamma, omega],))
U_val_at_end = U_val_at_end[:,0][1]
period_error = abs(U_val_at_start-U_val_at_end)
"""
Here we have an error of 2.0789e-06, which is essentially zero
So U_val_at_start = U_val_at_end
This is the periodic orbit that we later use for the shooting method
"""

"""
QUESTION 2
Shooting method requires a boundary value problem rather than an IVP
So we now consider the Duffing equation with condition: u(81.95) = u(81.95 + 2*pi) = U_val_at_start
To solve this with the shooting method, we instead consider the problem with 
initial conditions: u(81.95) = U_val_at_start, u'(81.95)=s to gain a parametric solution u(t;s)
We then want to find an s* s.t. u(81.95 + 2*pi;s*) = U_val_at_start in order to match the boundary
conditions given.
This is a root finding problem
let u(81.95 + 2*pi;s) - U_val_at_start = f(s)
We want to find the root of f(s*) = 0
We can do this using secant method
Note - didn't use Newtons method as that required a derivative of the function and 
we don't have the function explicitly.
"""

def funct_f(s):
    U0 = [U_val_at_start,s]
    Ts = [81.95,81.95 + 2*np.pi]
    solution = integrate.odeint(duffing,U0,Ts, args=([xi, gamma, omega],))
    solution = solution[:,0][1]
    return U_val_at_start - solution

def Secant_method(x_current, x_previous):
    x_next = x_current - ((x_current-x_previous)/(funct_f(x_current) - funct_f(x_previous)))*funct_f(x_current)
    return x_next

# Two starting estimates of s*: 0 and 1
x_current = 0
x_previous = 1

while abs(x_current - x_previous) > 1e-6:
    x_next = Secant_method(x_current,x_previous)
    x_previous = x_current
    x_current = x_next

s = x_current
# s = 0.2342497743735323

   
# Check: f(s) = 0
secant_solution = funct_f(s)
# This comes out as 6.4948e-14 which is essentially zero, so we know that our
# Secant method is working as expected.

#We check that  the estimated end value is the same as our calculated


Estimated_U_val_at_end = integrate.odeint(duffing,[U_val_at_start,s],[81.95,81.95+2*np.pi], args=([xi, gamma, omega],))
Estimated_U_val_at_end = Estimated_U_val_at_end[:,0][1] 
error_shooting = abs(Estimated_U_val_at_end - U_val_at_end)
#This gives us and error of 2.078e-06

 
## QUESTION 3
# Here w = 1.2 
# We want to find the periodic orbits

# This code below zooms in approximately on a periodic orbit
plt.figure(6)
plt.figure(figsize = (10,10))
omega = 1.2
plt.title("omega = 1.2")
plot_solution([1,0],'-r','')
plt.axhline(y=0, color='b', linestyle='-')
#plt.xlim(76.35,81.65)

# Note we have found one periodic orbit, but cannot simulate the other one as
# is it unstable
# Attempt - unstable as time goes forward = stable in negative time
def plot_solution_backwards(X0,colour,l):
    xs = np.linspace(0,-50*np.pi,10000)
    solution = integrate.odeint(duffing,X0,xs, args=([xi, gamma, omega],))
    ys = solution[:,0]
    plt.xlabel(r'$t$')
    plt.ylabel(r'$u$')
    plt.plot(xs,ys,colour, label = l)
    return solution

plt.figure(7)
plt.figure(figsize = (10,10))
omega = 1.2
plt.title("omega = 1.2")
plot_solution_backwards([1,0],'-r','')
plt.axhline(y=0, color='b', linestyle='-')
plt.xlim()
#However this doesn't give any periodic orbits as the stable orbit found earlier
# just blows up

