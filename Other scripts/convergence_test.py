# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 12:06:45 2018

@author: phoeb
"""
# We create the following test for convergence (a concatenated version of the 
# mathematical definition of convergence)
# This function finds the first point at which the error is less than an inputted 
# tolerance - epsilon, and then checks that all remaining entries in the
# vector of errors is also less than this tolerance
def test_convergence(sequence,epsilon,test_name):
    i=0
    flag = ""
    while sequence[i]>epsilon: # finding first point in the sequence where the error is < epsilon
        i = i+1
#Then need to check that all further entries in the sequence are less than epsilon too
    for j in range(i,len(sequence)):
        if sequence[j]>epsilon:
            flag = "N"
    if flag == "N":
        print("Test Failed: " + test_name)
    else: 
        print("Test Passed: " + test_name)
        