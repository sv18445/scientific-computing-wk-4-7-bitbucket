# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 11:58:58 2018

@author: phoeb
"""
import numpy as np
from week5_chebtests import runtests_cheb
from convergence_test import test_convergence
import matplotlib.pyplot as plt 
from scipy.optimize import fsolve
import math
from week5_odetests import runtests_ode

## Question 1 - generating the Chebyshev matrix
def c(i,N): # calculating c
    if i == 0 or i == N:
        c = 2
    else: c = 1
    return c

def collocation_points(N): # these are the Chebyshev points
    x = np.array([np.cos((j*np.pi)/N) for j in range(N+1)])
    return x
   
def chebyshev(N): # Making Chebyshev matrix of dimension (N+1)x(N+1)
    if N == 0:
        M = 0
    else:
        x = collocation_points(N)
        M = np.zeros(shape=(N+1,N+1))
        M[0][0] = (1/6)*((2*N**2)+1)
        M[N][N] = -(1/6)*((2*N**2)+1)
        for j in range(1,N):
            M[j][j] = (-x[j])/((2*(1-(x[j])**2)))
        for j in range(0,N+1):
            for i in range(0,N+1):
                if i != j:
                    M[i][j] = ((c(i,N)/c(j,N))*(((-1)**(i+j))/(x[i] - x[j])))
    return M


runtests_cheb(chebyshev)
#Passes all tests, so I know my chebyshev matrix is correct

## Question 2    
# Differentiating sin(x) in the interval [-1,1]
N = 10 # first try with N=10
x = collocation_points(N)
y = [np.sin(x[i]) for i in range(N+1)]
D = chebyshev(N)
Dy = D@y
plt.figure(1)
plt.plot(x,Dy, label = "Estimated solution")

#Actual solution
x_many = np.linspace(-1,1,1000)
actual_y = [np.cos(x_many[i]) for i in range(1000)]
plt.plot(x_many,actual_y, label = "Actual solution")
plt.xlim(-1,1)
plt.legend()
plt.title("Derivative of sinx between -1 and 1")

# We can now apply a numerical test on how close the two solutions are
def difference_against_cos(N):
    x = collocation_points(N) 
    actual = [np.cos(x[i]) for i in range(N+1)]
    D = chebyshev(N)
    y = [np.sin(x[i]) for i in range(N+1)]
    calculated = D@y
    cos_error = np.linalg.norm(calculated-actual)
    return cos_error

# But we want to test for convergence explicitly
# This function generates a vector of errors with error_vector[i] being the 
# error when N=i
def cos_errors(N):
    error_vector = []
    for i in range(1,N+1):
        error_vector.append(difference_against_cos(i))
    return error_vector

test_convergence(cos_errors(100),0.001, "Sine") # Chosen N=100 because higher N slows down the program

## Second example - differentiating f(x) = x^5 + 3x^4 + 3 on interval [-1,1]
# We know that the solution is 5x^4 + 12x^3

def f(x):
    return (x**5)+(3*x**4) + 3

def f_diff(x):
    return (5*x**4)+(12*x**3)

N = 10 # first try with N=10
x = collocation_points(N)
y = [f(x[i]) for i in range(N+1)]
D = chebyshev(N)
Dy = D@y
plt.figure(2)
plt.plot(x,Dy, label = "Estimated solution")

#Actual solution
x_many = np.linspace(-1,1,1000)
actual_y = [f_diff(x_many[i]) for i in range(1000)]
plt.plot(x_many,actual_y, label = "Actual solution")
plt.xlim(-1,1)
plt.legend()
plt.title("Derivative of f(x) = x^5 + 3x^4 +3")

# Again we can see that the two curves match up
def difference_against_f_diff_(N):
    x = collocation_points(N) 
    actual = [f_diff(x[i]) for i in range(N+1)]
    D = chebyshev(N)
    y = [f(x[i]) for i in range(N+1)]
    calculated = D@y
    f_diff_error = np.linalg.norm(calculated-actual)
    return f_diff_error
    
def f_errors(N):
    error_vector = []
    for i in range(1,N+1):
        error_vector.append(difference_against_f_diff_(i))
    return error_vector

test_convergence(f_errors(100),0.0001, "f(x)") # Note if you make epsilon too small
#the test may fail despite convergence occuring, you would need to make N bigger
# when you call f_errors

##Question 3
N = 20
# In this question we want to solve x' = sin(pi*t) - x
# We use t as the collocation points and solve for x

def my_collocation_code(ode, N, x0, pars):
    t = collocation_points(N)
    def system_to_solve(x):
        D = chebyshev(N)
        Dx = D@x
        system = Dx - ode(x,t,pars)
        system = system[0:-1]
        BCs = x[0] - x[-1]
        system = np.append(system, BCs)
        return system
    
    solution = fsolve(system_to_solve,x0)
    return solution

def ode(x,t,pars):
    return np.sin(np.pi*t) - x

t = collocation_points(N)
x0 = [1 for i in range(len(t))]
# Here we plot the solution of the ode x' = sin(pi*t) - x and the estimated solution
# Estimated solution
plt.figure(3)
solution = my_collocation_code(ode, N, x0, [])
plt.plot(t,solution, "r", label = "Estimated solution")

# Actual solution
t_lots = np.linspace(-1,1,5000)
A = 1/(1+(np.pi)**2)
actual_sol = [A*(math.sin(np.pi*t_lots[i]) - np.pi*math.cos(np.pi*t_lots[i])) for i in range(len(t_lots))]
plt.plot(t_lots,actual_sol, "b", label = "Actual solution")
plt.legend()
plt.title("N = 20 solutions to x' = sin(pi*t) - x")

# Running the given ODE test
runtests_ode(my_collocation_code)
