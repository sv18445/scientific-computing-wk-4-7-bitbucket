# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 22:02:10 2018

@author: phoeb
"""

import numpy as np
import math
from scipy import integrate
import matplotlib.pyplot as plt

## NOTE THIS ONLY WORKS FOR SECOND ORDER SYSTEMS AT THE MOMENT
# The user needs to fill in their system of differential equations into the system_of_DEs
# function in the python script
# Then when they run it, the script plots a graph of the solution

# The system that the user enters in system_of_DEs has to be in a specific form:
# Suppose the differential equation is n dimensional
# So has variables in an array X = [x1,x2,...,xn]
# Then the system is given by the array X' = [x1',x2',...,xn']
# Where ' denotes differentiation wrt t
# and each x1' is in terms of X[1],X[2],...X[n] and t
# the system is the n-dim array X'
# eg. [X[1], 0.2*math.sin(t) - 2*0.05*X[1] - X[0] - (X[0])**3]  (Duffing equation)
def system_of_DEs(X,t):
    return [X[1], 0.2*math.sin(t) - 2*0.05*X[1] - X[0] - (X[0])**3]  

# The user also needs to input their boundary values below
BCs = [0,2*np.pi]
boundary_x_values = 0

#User also inputs our two estimates for the secant method, in the form [v1,v2]
starting = [0,1]

# Now we can start our code (no user input needed from this point)
def plot_solution(X0):
    xs = np.linspace(0,50*np.pi,10000)    
    solution = integrate.odeint(system_of_DEs,X0,xs)
    ys = solution[:,0]
    plt.figure(figsize = (10,10))
    plt.xlabel(r'$t$')
    plt.ylabel(r'$u$')
    plt.plot(xs,ys)
    plt.title("Solution for given BVP")
    return solution

def funct_f(s):
    X0 = [boundary_x_values,s]
    Xs = BCs
    solution = integrate.odeint(system_of_DEs,X0,Xs)
    solution = solution[:,0][1]
    return solution

def Secant_method(x_current, x_previous):
    x_next = x_current - ((x_current-x_previous)/(funct_f(x_current) - funct_f(x_previous)))*funct_f(x_current)
    return x_next

def finding_s(x_current,x_previous):
    while abs(x_current - x_previous) > 1e-6:
        x_next = Secant_method(x_current,x_previous)
        x_previous = x_current
        x_current = x_next
    return x_current
        
s = finding_s(starting[0],starting[1])
plot_solution([0,s])

