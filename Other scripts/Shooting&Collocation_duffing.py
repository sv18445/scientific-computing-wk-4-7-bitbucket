# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 10:26:24 2018

@author: phoeb
"""
import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt 
from scipy import integrate

def mass_spring(X,t,pars): # setting up differential equation x'' + 2*xi*x' + kx = sin(pi*t)
    return [X[1],np.sin(np.pi*t) - 2*pars[0]*X[1] - pars[1]*X[0]]

# defining parameters:
xi = 0.05
gamma = 0.5
beta = 1
k=1 
pars = [xi, k, gamma, beta]

T = 2 # We know the period is 2 for both mass_spring and duffing

## Shooting code
def zero_problem(s, ode, T, pars): # this is our root finding problem
    int_over_T = integrate.odeint(ode,s,[-1,T-1], args=(pars,))
    last_x = int_over_T[-1,:] # takes the last value in the period (it a vector containing x(T) and x'(T))
    return s - last_x

def shooting(ode, sol_guess, T, pars):
    solution = fsolve(zero_problem,sol_guess, args = (ode, T, pars,))
    return solution
## Shooting code end

## Collocation start
N = 50

def c(i,N): # calculating c
    if i == 0 or i == N:
        c = 2
    else: c = 1
    return c

def collocation_points(N): # these are the Chebyshev points
    x = np.array([np.cos((j*np.pi)/N) for j in range(N+1)])
    return x
   
def chebyshev(N): # Making Chebyshev matrix of dimension (N+1)x(N+1)
    if N == 0:
        M = 0
    else:
        x = collocation_points(N)
        M = np.zeros(shape=(N+1,N+1))
        M[0][0] = (1/6)*((2*N**2)+1)
        M[N][N] = -(1/6)*((2*N**2)+1)
        for j in range(1,N):
            M[j][j] = (-x[j])/((2*(1-(x[j])**2)))
        for j in range(0,N+1):
            for i in range(0,N+1):
                if i != j:
                    M[i][j] = ((c(i,N)/c(j,N))*(((-1)**(i+j))/(x[i] - x[j])))
    return M

def my_collocation_code(ode, N, sol_guess, pars):
    #t = collocation_points(N)
    def system_to_solve(x): # This is the function that throws up the error
        D = chebyshev(N)
        #x = np.array([x])
        x = np.reshape(x,(51,2))
        Dx = D@x
        BC1 = [x[0][0] - x[-1][0]] 
        BC2 = [x[0][1] - x[-1][1]] # since it is a 2nd order problem, need two sets of BCs
        system1 = Dx[0] - ode(x,t,pars)[0]
        system1 = system1[0:-1]
        system1 = np.append(system1, BC1)
        system2 = Dx[1] - ode(x,t,pars)[1]
        system2 = system2[0:-1]
        system2= np.append(system2, BC2)
        system = np.array([[system1[i],system2[i]] for i in range(len(system1))])
        return system
    solution = fsolve(system_to_solve,sol_guess)
    return solution
## collocation code end
    
# Plotting collocation soln
t = collocation_points(N)
solution_col = my_collocation_code(mass_spring, N, np.array([[1,1] for i in range(N+1)]), pars)
plt.plot(t,solution_col, label = "Collocation Soln")

#Plotting shooting soln
x0 = shooting(mass_spring, [0,1], T, pars) 
t = np.linspace(-1,T-1,1000)
solution_shooting = integrate.odeint(mass_spring, x0, t, args = (pars,))
solution_shooting = solution_shooting[:,0]
plt.plot(t,solution_shooting, label = "Shooting Soln")
plt.legend()


