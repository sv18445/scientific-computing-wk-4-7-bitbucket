# -*- coding: utf-8 -*-
"""
Created on Sat Nov 17 13:57:20 2018

@author: phoeb
"""
"""
Shooting method
Our original code used the secant method for root finding. But I have now realised that 
fsolve is a better option
Here we rewrite the shooting code
"""

import numpy as np
import math
from scipy import integrate
from scipy.optimize import fsolve

def duffing(X,t,pars):
    return [X[1], pars[1]*math.sin(pars[2]*t) - 2*pars[0]*X[1] - X[0] - (X[0])**3]

# defining parameters:
xi = 0.05
gamma = 0.2
omega = 1.2
T = 2*np.pi

def funct_f(s, ode, T, pars): # this is our root finding problem
    int_over_T = integrate.odeint(ode,s,[0,T], args=(pars,))
    last_x = int_over_T[-1,:] # takes the value in the period (both x and x')
    return s - last_x

def shooting(ode, sol_guess, T, pars):
    solution = fsolve(funct_f,sol_guess, args = (ode, T, pars))
    return solution

x0 = shooting(duffing, [1,0], T, [xi,gamma,omega])
# Shooting method compared with given shooting code and it works
# Shooting function returns a 2d array [x,x'] with the initial conditions of the
# periodic orbit