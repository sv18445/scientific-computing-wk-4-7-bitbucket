# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 11:52:35 2018

@author: phoeb
"""
import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt 

"""
Q1 - Natural parameter continuation
 x^3 - x + c = 0 
 c between [-2,2]
"""

def f(x,c):
    return x**3 - x + c

def predict(current, previous): # note current_point and previous_point must be numpy arrays
    next_pt = 2*current - previous
    return next_pt
    
def corrected(point):
    c = point[1]
    x0 = point[0]
    xsolution = fsolve(f,x0,args = c)
    corrected = np.array([float(xsolution),c])
    return corrected
    
x_sol_array = []
c_array = []

c = -2
x_previous = fsolve(f,1,args = c)
previous = np.array([float(x_previous), c])
x_sol_array.append(x_previous)
c_array.append(c)

c = -1.9
x_current = fsolve(f,1,args = c)
current = np.array([float(x_current), c])
x_sol_array.append(x_current)
c_array.append(c)

h = 0.1
num_iterations = 4/h
i = 1
while i < num_iterations:
    next_pt = predict(current, previous)
    next_pt = corrected(next_pt)
    previous = current
    current = next_pt
    x_sol_array.append(next_pt[0])
    c_array.append(next_pt[1])
    i = i+1
    
plt.figure(0)
plt.plot(c_array, x_sol_array)
plt.title("Natural parameter continuation for $f(x,c) = x^3 - x + c$")
plt.xlabel("c")
plt.ylabel("x values s.t. f(x,c) = 0")
plt.show()
