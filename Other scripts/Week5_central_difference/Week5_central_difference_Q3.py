# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 16:12:22 2018

@author: phoeb
"""
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

omega = 1

def central_difference_matrix(h,N):
    M = np.zeros(shape=(N,N))
    if N <= 0:
        print("N must be a positive integer")
    elif N == 1:
        M[0][0] = 0
    else: 
        M[0][-1] = -1
        M[-1][0] = 1
        for i in range(N-1):
                M[i][i+1] = 1
                M[i+1][i] = -1
    return M/(2*h)

def generate_derivative_estimate(x):
    D = central_difference_matrix(h,len(t))
    Dx = D@x
    return Dx

def system_to_solve(x):
    Dx = generate_derivative_estimate(x)
    return Dx - np.sin(omega*t) + x

t = np.linspace(0,2*np.pi,500)
h = 2*np.pi/500
x0 = [1 for i in range(len(t))]

solution = fsolve(system_to_solve,x0)
plt.plot(t,solution)

A = 0 # Here we have A=0 because the finite difference matrix imposes that x(0) = x(2*pi)
t_lots = np.linspace(0,2*np.pi,1000)
actual_sol = [0.5*math.sin(t_lots[i])-0.5*math.cos(t_lots[i])+A*math.e**(-t_lots[i]) for i in range(len(t_lots))]
plt.plot(t_lots,actual_sol)
