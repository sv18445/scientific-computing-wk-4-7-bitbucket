# -*- coding: utf-8 -*-
"""
Created on Sat Nov  3 17:27:30 2018

@author: phoeb
"""
import numpy as np

def central_difference_matrix(h,N):
    M = np.zeros(shape=(N,N))
    if N <= 0:
        print("N must be a positive integer")
    elif N == 1:
        M[0][0] = 0
    else: 
        M[0][-1] = -1
        M[-1][0] = 1
        for i in range(N-1):
                M[i][i+1] = 1
                M[i+1][i] = -1
    return M/(2*h)


def testeq(M, N, name):
    """
    testeq(M, N, name)

    Test for equality of two matrices (M and N) allowing for some floating point
    error. The parameter name is the name of the test.
    """
    D = abs(M - N)
    if all(D < 1e-6):
        print("Passed " + name)
    else:
        print("Failed " + name)

D1 = central_difference_matrix(1,2)
M1 = 1/2*np.array([[0,1],[-1,0]])
testeq(D1,M1,"h = 1, N = 2")

D2 = central_difference_matrix(2,3)
M2 = 1/4*np.array([[0,1,-1],[-1,0,1],[1,-1,0]])
testeq(D2,M2,"h = 2, N = 3")
