# -*- coding: utf-8 -*-
"""
Created on Sat Nov  3 18:15:50 2018

@author: phoeb
"""
import numpy as np
import math
import matplotlib.pyplot as plt

#Use the Chebyshev differentiation matrix to differentiate known functions over some interval (e.g., sin(x) for −1 ≤x≤ 1).
#Write a number of tests that your code should satisfy and ensure that your code does satisfy them.

def central_difference_matrix(h,N):
    M = np.zeros(shape=(N,N))
    if N <= 0:
        print("N must be a positive integer")
    elif N == 1:
        M[0][0] = 0
    else: 
        M[0][-1] = -1
        M[-1][0] = 1
        for i in range(N-1):
                M[i][i+1] = 1
                M[i+1][i] = -1
    return M/(2*h)

# x' = Dx
    
# First test - differentiating sin(x) in the interval [0,2pi]
# try with h = pi/100
# first we make a vector of times: t[i] = h*i up to t=2pi
# first we make our vector x of the value of sin at these times and then premultiply by
# our differentiation matrix
def sin_differentiation(h):
    t_max = 2*np.pi/h + 1
    t_max = int(round(t_max))
    t = [(i*h) for i in range(t_max)]
    x_row = np.array([math.sin(t[i]) for i in range(len(t))])
    x_column = np.transpose([x_row])
    D = central_difference_matrix(h,len(t))
    x_diff = D@x_column
    x_diff_again = D@x_diff
    x_diff_again[0] = 1
    x_diff_again[-1] = 1
    return t,x_diff_again
# We have to prescribe x_diff at the endpoints because the central difference method
# relies on the x_i-1 and x_i+1 which don't exist at the endpoints

h = np.pi/5
values = sin_differentiation(h)
plt.figure(0)
plt.plot(values[0],values[1])

ts_accurate = np.linspace(0,4*np.pi,100000)
xs_accurate = [math.cos(ts_accurate[i]) for i in range(len(ts_accurate))]
plt.plot(ts_accurate,xs_accurate)
plt.xlim(0,2*np.pi)
plt.title("sin(x) - Actual solution (orange) and estimated (blue) for h=pi/5")
# This is a good visual test that our code works

# We can now apply a numerical test on this for smaller h
def difference_against_cos(h):
    calc = sin_differentiation(h)[1]
    t = sin_differentiation(h)[0]
    actual = [math.cos(t[i]) for i in range(len(t))]
    actual = np.transpose([actual])
    difference = np.linalg.norm(calc-actual)
    return str(difference)
print("sin(x) example:")
print("h = pi, error is: " +difference_against_cos(np.pi))
print("h = pi/2, error is: " +difference_against_cos(np.pi/2))
print("h = pi/4, error is: " +difference_against_cos(np.pi/4))
print("h = pi/100, error is: " +difference_against_cos(np.pi/100))
# Error decreasing as h gets smaller
# We know the solutions match as the error is tending to zero


##Second test differentiating f(x) = x^5 + 3x^4 + 3 between x = 0 and x = 10
# We know that the solution is 5x^4 + 12x^3
def f(x):
    return (x**5)+(3*x**4) + 3

def f_diff(x):
    return (5*x**4)+(12*x**3)

def f_differentiation(h):
    t_max = 10/h + 1
    t_max = int(round(t_max))
    t = [(i*h) for i in range(t_max)]
    x_row = np.array([f(t[i]) for i in range(len(t))])
    x_column = np.transpose([x_row])
    D = central_difference_matrix(h,len(t))
    x_diff = D@x_column
    x_diff[0] = 0
    x_diff[-1] = 62000
    return t,x_diff

plt.figure(1)
h = 2
values = f_differentiation(h)
plt.plot(values[0],values[1])

ts_accurate = np.linspace(0,4*np.pi,100000)
xs_accurate = [f_diff(ts_accurate[i]) for i in range(len(ts_accurate))]
plt.plot(ts_accurate,xs_accurate)
plt.xlim(0,10)
plt.title("f(x) - Actual solution (orange) and estimated (blue) for h=2")

def difference_against_f_differentiation(h):
    calc = f_differentiation(h)[1]
    t = f_differentiation(h)[0]
    actual = [f_diff(t[i]) for i in range(len(t))]
    actual = np.transpose([actual])
    difference = np.linalg.norm(calc-actual)
    return str(difference)

print("f(x) = x^5 + 3x^4 + 3 example:")
print("h = 5, error is: " +difference_against_f_differentiation(5))
print("h = 2, error is: " +difference_against_f_differentiation(2))
print("h = 0.1, error is: " +difference_against_f_differentiation(0.1))
print("h = 0.001, error is: " +difference_against_f_differentiation(0.001))

## Note in both these cases I fixed the endpoints to have the correct derivative,
# Had I not done this, there would always be some error between the estimate
# and actual derivative